#Snap BobSay 0.1.0

Snap BobSay is a fun example program using the \Snap\Say package to create PHP CLI programs in the style of cowsay.

Using the Snap\Say package, you too can easily make your own "say" program with PHP!

##Installation
This library requires PHP 5.4 or later; we recommend using the latest available version of PHP as a matter of principle.
It is installable and autoloadable via Composer as snap/bobsay.
```bash
composer require snap/bobsay
```
For best results, install the project into a directory that is in your include path, for example in Linux or Mac, this can be ~/bin

The called file, bobsay.php needs to be moved to the same level as the project directory, and on Linux/Mac it will need to be made executable can optionally be renamed to bobsay
Windows users will need to retain the .php extension.
```bash
cd vendor/snap/bobsay/src
chmod +x bobsay.php
mv bobsay.php ../../../../bobsay
```
Note: bobsay will now be in the same directory as composer.json and vendor.

##Use Examples
If you are in the directory with the bobsay file, you can run
```bash
#Linux/Mac
./bobsay
#Windows
php ./bobsay.php
```
On Linux/Mac, assuming you followed the instructions above and the bobsay file is in a directory in your include path, you can call the program from anywhere.
```bash
# random quote
bobsay
# tell Bob what to say
bobsay "My name is Bob and I think you are awesome!"
# pipe output from another program into Bob's mouth (you must install fortune for this example)
fortune | bobsay
```
On Windows, you will need to either be in the same directory as the bobsay.php file or provide an absolute path to it.
```bash
# random quote
php bobsay.php
# tell Bob what to say
php C:\path\to\bobsay.php "My name is Bob and I think you are awesome!"
```

##Troubleshooting
Permission denied - make sure you made the bobsay/bobsay.php file executable

bobsay: command not found - the bobsay file is not in your include path.  Add its parent directory to your include path, call it from within its parent directory or use an absolute path to it.

PHP not found - make sure PHP is installed properly by running php -v  If it isn't install it using your package manager.

Other questions or problems: Feel free to contact me through AlexFraundorf.com and I'll do my best to help :-)

Happy PHPing!
