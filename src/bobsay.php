#!/usr/bin/php
<?php
/*
 * Command script for the fun cowsay style command line program of Bob's quotes.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2019, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\BobSay
 * @version 0.1.0 02/21/2019
 * @since 0.1.0 02/21/2019
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\BobSay;

require_once(__DIR__ . '/vendor/autoload.php');

$Bob = new Bob();

$Bob
        // set the path to Bob's quotes
        ->setPathToQuotes(__DIR__ . '/vendor/snap/bobsay/src/quotes.txt')
        // run the program passing the $argv array
        ->run($argv)
        ;
